# How to use

Settings for mercurial.ini:

~~~~
[tortoisehg]
workbench.revdetails.custom-menu = GenerateJiraMessage
 
[extensions]
genjiramsg = C:\Devtools\HGExt\genjiramsg.py
 
[tortoisehg-tools]
GenerateJiraMessage.command = hg genjiramsg {REV} {REVID}
GenerateJiraMessage.enable = istrue
GenerateJiraMessage.icon = copy-hash
GenerateJiraMessage.showoutput = True
~~~~