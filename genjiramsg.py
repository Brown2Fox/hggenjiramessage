"""genjiramsg
"""

import os, sys, subprocess
from mercurial import cmdutil, templatekw, commands
from mercurial import registrar
from mercurial.i18n import _

cmdtable = {}
command = registrar.command(cmdtable)


def getRepoName():
    args = ["hg", "paths", "default"]
    process = subprocess.Popen(args, stdout=subprocess.PIPE)
    tp = process.communicate()
    return tp[0].replace('\n','')

def addToClipBoard(s):
    if sys.platform == 'win32' or sys.platform == 'cygwin':
        subprocess.Popen(['clip'], stdin=subprocess.PIPE).communicate(s.decode('cp1251').encode('cp866'))
    else:
        raise Exception('Platform not supported')


@command('genjiramsg', [], _('[options] revision changeset'))
def genjiramsg(ui, repo, revision, changeset, **opts):

    ctx = repo[int(revision)]

    repo = getRepoName()
    if (repo[-1] != "/"):
        repo += "/"
    repo += "rev/" + changeset

    msg = 'h4. br {}, rev {}, cset [{}|{}] \n- {}\n'.format(ctx.branch(), revision, changeset, repo, ctx.description())

    addToClipBoard(msg)
    ui.write(msg)
